﻿using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

namespace Covid.UI
{
    public interface IPopupView
    {
        TextMeshProUGUI TitleText { get;}
        TextMeshProUGUI BodyText { get;}
        List<Button> ButtonsList { get; }
    }
}
