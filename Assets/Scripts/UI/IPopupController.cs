﻿using System;
using System.Collections.Generic;
using System.Security.Cryptography.X509Certificates;
using UnityEngine;

namespace Covid.UI
{
    public interface IPopupController
    {
        void InitPopup(string title, string body, Dictionary<string, Action> btnActions);
    }
}