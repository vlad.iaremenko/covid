﻿using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Covid.UI
{
    public class PopupView : MonoBehaviour,IPopupView
    {
        [SerializeField] private TextMeshProUGUI _titleText;
        [SerializeField] private TextMeshProUGUI _bodyText;
        [SerializeField] private List<Button> _buttons;

        public TextMeshProUGUI TitleText => _titleText;
        public TextMeshProUGUI BodyText => _bodyText;
        public List<Button> ButtonsList => _buttons;
    }
}
