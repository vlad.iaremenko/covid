﻿using System;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Covid.UI
{
    public class PopupController : MonoBehaviour, IPopupController
    {
        private PopupView _popupView;

        private void Awake()
        {
            if(_popupView==null)
                _popupView = GetComponent<PopupView>();
        }
        
        public void InitPopup(string title, string body, Dictionary<string, Action> btnActions)
        {
            if (btnActions.Count == 0 || btnActions.Count > 5)
            {
                throw new Exception("Wrong number of actions to assign. Must be [1..5]");
            }
            
            _popupView.TitleText.text = title;
            _popupView.BodyText.text = body;
            
            for (var i = 0; i < _popupView.ButtonsList.Count; i++)
            {
                _popupView.ButtonsList[i].gameObject.SetActive(false);
            }
            
            for (var i = 0; i < btnActions.Count; i++)
            {
                var btn =  _popupView.ButtonsList[i];
                btn.gameObject.SetActive(true);
                btn.GetComponentInChildren<TextMeshProUGUI>().text = btnActions.Keys.ElementAt(i);
                btn.onClick.AddListener(btnActions.ElementAt(i).Value.Invoke);
            }
        }

        private void OnDestroy()
        {
            for (var i = 0; i <  _popupView.ButtonsList.Count; i++)
            {
                _popupView.ButtonsList[i].onClick.RemoveAllListeners();
            }
        }
    }
}
