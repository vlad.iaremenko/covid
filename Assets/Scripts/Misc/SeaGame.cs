﻿using System;
using System.Collections.Generic;
using System.Text;
using UnityEngine;

namespace Covid.Misc
{
    public class SeaGame : MonoBehaviour
    {
        private Board _board = new Board();
        
        private void Start()
        {
            for (int i = 0; i < _board.Cells.GetLength(0); i++)
            {
                for (int j = 0; j < _board.Cells.GetLength(1); j++)
                {
                    _board.Cells[i, j] = new Cell();
                }
            }

            var ship = new Ship();
            ship.ID = 1;
            
            ship.Cells.Add(_board.Cells[2, 2]);
            ship.Cells.Add(_board.Cells[2, 3]);
            ship.Cells.Add(_board.Cells[2, 4]);
            ship.Cells.Add(_board.Cells[2, 5]);
            
            ship.InitShipCells();
            
            ship = new Ship();

            ship.ID = 2;
            ship.Cells.Add(_board.Cells[4, 7]);
            ship.Cells.Add(_board.Cells[5, 7]);
            _board.Cells[5, 7].MineExists = true;
            ship.Cells.Add(_board.Cells[6, 7]);

            ship.InitShipCells();
            
            ship = new Ship();

            ship.ID = 3;
            ship.Cells.Add(_board.Cells[7, 2]);
            ship.Cells.Add(_board.Cells[8, 2]);
            ship.Cells.Add(_board.Cells[9, 2]);
            
            _board.Cells[7, 2].MineExists = true;
            _board.Cells[8, 2].MineExists = true;
            _board.Cells[9, 2].MineExists = true;

            ship.InitShipCells();
            
            UpdateBoard();
        }

        private void UpdateBoard()
        {
            var sb = new StringBuilder();
            
            sb.Append("\n");

            for (int i = 0; i < _board.Cells.GetLength(0); i++)
            {
                for (int j = 0; j < _board.Cells.GetLength(1); j++)
                {
                    var sign = "_";

                    var cell = _board.Cells[i, j];
                    
                    if (cell.ParentShip != null)
                    {
                        sign = cell.ParentShip.ID.ToString();
                        
                        if(cell.MineExists)
                            sign = "#";

                        if (cell.ParentShip.IsDestroyed())
                            sign = "_";
                    }
                        
                    sb.Append(sign);
                    sb.Append("  ");
                }
                sb.Append("\n");
            }

            Debug.Log(sb);
        }

        private class Ship
        {
            public List<Cell> Cells = new List<Cell>();
            public int ID;

            public void InitShipCells()
            {
                foreach (var cell in Cells)
                {
                    cell.ParentShip = this;
                }
            }

            public bool IsDestroyed()
            {
                foreach (var cell in Cells)
                {
                    if (!cell.MineExists)
                        return false;
                }

                return true;
            }
        }
        
        private class Board
        {
            public Cell[,] Cells = new Cell[10, 10];
        }
        
        private class Cell
        {
            public bool MineExists;
            public Ship ParentShip;
            public int SubmarineID;
        }
    }
}
