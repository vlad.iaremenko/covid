﻿using System;
using UnityEngine;

namespace Covid.Misc
{
    public class Serializer : MonoBehaviour
    {
        private void Start()
        {
            var data = new UserData() {ID = "Vlad", Age = 27};
            var key = "PlayerData";

            //Save(data,key);
            var loadedData = Load<UserData>(key);
            //Debug.Log(loadedData.ID);
        }

        public static void Save<T>(T data, string dataKey)
        {
            try
            {
                var json = JsonUtility.ToJson(data);
                PlayerPrefs.SetString(dataKey,json);
            }
            catch (Exception e)
            {
                Debug.LogError(e);
                throw;
            }
        }
        
        public static T Load<T>(string dataKey)
        {
            try
            {
                var json = PlayerPrefs.GetString(dataKey);
                return JsonUtility.FromJson<T>(json);
            }
            catch (Exception e)
            {
                Debug.LogError(e);
                throw;
            }
        }
    }


    public class UserData
    {
        public string ID;
        public int Age;
    }
}
