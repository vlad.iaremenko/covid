﻿using System;
using System.Collections.Generic;
using Covid.UI;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Covid.Misc
{
    public class SceneLoader : MonoBehaviour
    {
        private void Start()
        {
            var actionsList = new Dictionary<string, Action>();
            
            actionsList.Add("Btn1",()=>{Debug.Log("Btn1");});
            actionsList.Add("Btn2",()=>{Debug.Log("Btn2");});
            actionsList.Add("Btn3",()=>{Debug.Log("Btn3");});
            
            
            var prefab = Resources.Load<PopupController>("Popup");
            Instantiate(prefab,transform).InitPopup("Title","Body",actionsList);
        }

        /*
        public class Microbe
        {
            private int _currentHealth;
            private MicrobesView _microbesView;

            public void UpdateMicrobeHealth(int newHealthValue)
            {
                _microbesView.TotalHealth = newHealthValue - _currentHealth;
                _currentHealth = newHealthValue;
                _microbesView.RefreshView();
            }
        }
        
        public class MicrobesView
        {
            [SerializeField] private List<Microbe> _microbes;
            [SerializeField] private TextMeshProUGUI _infoText;
            public int TotalHealth { get; set; }

            public void RefreshView()
            {
                if (_microbes.Count == 0)
                {
                    _infoText.text = "No microbes";
                    return;
                }
                
                _infoText.text = $"Total microbes: {_microbes.Count} Avg health {TotalHealth / _microbes.Count}";
            }
        }
        */
        
        
        public class MicrobesView : MonoBehaviour
        {
            [SerializeField] private List<Microbe> _microbes;
            [SerializeField] private TextMeshProUGUI _infoText;
            private float _totalHealth;
            private int _activeMicrobes;

            void Update()
            {
                if (_microbes.Count == 0)
                {
                    _infoText.text = "No microbes";
                    return;
                }

                _totalHealth = 0f;
                _activeMicrobes = 0;

                //For loop is faster then Linq Sum.
                for (var i = 0; i < _microbes.Count; i++)
                {
                    if (_microbes[i] == null)
                        continue;

                    _totalHealth += _microbes[i].Health;
                    _activeMicrobes++;
                }

                _infoText.text = $"Total microbes: {_microbes.Count} Avg health {_totalHealth / _activeMicrobes}";
            }
        }

        public class Microbe
        {
            public float Health;
        }

    }
}
